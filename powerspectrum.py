'''
powerspectrum.py
by Oscar Hernandez 2019

This computes the angular power spectrum of our maps and plots the spectrum.
The 'azimuthal_average' function below is based on the radialProfile.py script
publically available in AsroBetter and many other many places:
http://www.astrobetter.com/wiki/python_radial_profiles
https://image-tools.readthedocs.io/en/latest/_modules/image_tools/radialprofile.html
https://github.com/mkolopanis/python/blob/master/radialProfile.py
https://github.com/keflavich/image_tools/blob/master/image_tools/radialprofile.py

'''

import numpy as np
import numpy.fft as fft
import header
import matplotlib.pyplot as plt

TCMB = header.TCMB
arcmin = header.arcmin

def azimuthal_average(image, resolution=1.0, center=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is 
             None, which then uses the center of the image (including 
             fracitonal pixels).

    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if not center:
        center = np.array([(x.max() - x.min()) / 2.0, (y.max() - y.min()) / 2.0])
 
    r = np.hypot(x - center[0], y - center[1])
    
    # Get sorted radii
    ind = np.argsort(r.flat)
    r_sorted = r.flat[ind]
    i_sorted = image.flat[ind]

    # Get the integer part of the radii (bin size = 1)
    # r_int = r_sorted.astype(int)
    r_int = np.rint(r_sorted).astype(int)  # r_int is r rounded to nearest integer
    # this binning allows for an estimate of power spectrum with one single map
    # The len(r_int)=number of pixels = N^2
    # r_int_max = np.sqrt((center**2).sum()) = 361.33 for 512x512 pixels
    # r_int_min = np.sqrt(1/2) = 0.707

    # Find all pixels that fall within each radial bin.
    deltar = r_int[1:] - r_int[:-1]  # Assumes all radii represented
    # len(deltar) = -1 + len(r_int)
    rind = np.where(deltar)[0]  # location of changed radius.
    # The array length of rind
    # = -1+ distance from center to corner = -1+(N-1)/sqrt(2)= 360 for 512x512 pixels
    # instead of the array length of deltar = N^2-1
    nr = rind[1:] - rind[:-1]  # number of radius bin.
    # len(nr)=-1+len(rind) = 359 for 512x512 pixels

    # k is the wave number here.
    # arcmin=np.pi/180/60; resolution = arcmin for CMBEDGE, 0.42*arcmin for Ringeval
    length = resolution * image.shape[0]
    # k_sorted = (2*np.pi/length)*r_sorted
    # k_int = np.rint(k_sorted).astype(int)
    k_int_rind = np.rint((2 * np.pi / length) * r_int[rind]).astype(int)
    # len(k_int_rind)= len(rind) = 360 for 512x512 pixels

    # Cumulative sum to figure out sums for each radius bin
    csim = np.cumsum(i_sorted, dtype=float)
    tbin = csim[rind[1:]] - csim[rind[:-1]]

    # from above line we see that tbin and radial_prof[i] correspond to rind and k_int_rind[i+1]

    radial_prof = tbin / nr  # len(radial_profile)=len(nr) = 359 for 512x512 pixels

    return [radial_prof, nr, k_int_rind]

# maps has size [n_maps, size, size], this will return an array of length 
# [n_maps, floor(size/2)] containing the power spectrum of each map
def power_spectrum(maps,resolution=1.0):
    """
    given an array of maps of size (n_maps, length, length)
    this produces the power spectrum of each of the n_maps,
    """
    power_spectra = []
    Nr=[]
    elles=[]

    map_size = maps.shape[1]
    volume = (resolution * map_size) ** 2
    fft_maps = resolution ** 2 * fft.fftshift(fft.fft2(maps))
    fft_sq_maps = np.abs(fft_maps) ** 2

    # Do first map to get Nr and elles
    azimuthalAve , nr, el = azimuthal_average(fft_sq_maps[0],resolution)
    power_spectra.append(azimuthalAve/volume)
    Nr.append(nr)
    elles.append(el)
    # Do the rest of the maps. Nr and elles are exactly the same
    for i in range(1, maps.shape[0]):
        azimuthalAve , nr, el = azimuthal_average(fft_sq_maps[i],resolution)
        power_spectra.append(azimuthalAve/volume)
        # Nr.append(nr)
        # elles.append(el)  

    elles = np.array(elles)
    elles_sq_over_2pi = (elles[0, 1:] * (elles[0, 1:] + 1)) / (2 * np.pi)

    # return [np.array(power_spectra),np.array(Nr),np.array(elles)]
    return [np.array(power_spectra), np.array(Nr), elles, elles_sq_over_2pi]

def plot_C_l_rescaled(maps, resolution):
    power, nr, elles, elles_sq_over_2pi = power_spectrum(maps, resolution)
    C_l_rescaled = TCMB*TCMB * elles_sq_over_2pi * power
    C_l_rescaled_std = C_l_rescaled.std(axis = 0)
    C_l_rescaled_mean = C_l_rescaled.mean(axis = 0)
    dispall = input("Display power spectrum of each individual map? [y/n]: ")

    # semilog plots
    if dispall.lower() == "y":
        for i in range(C_l_rescaled.shape[0]):
            plt.semilogx(elles[0, 1:], C_l_rescaled[i])
    plt.semilogx(elles[0, 1:], C_l_rescaled_mean, label = "mean")
    plt.fill_between(elles[0, 1:], C_l_rescaled_mean +  C_l_rescaled_std, C_l_rescaled_mean -  C_l_rescaled_std, alpha = 0.4, label = "1$\sigma$")
    plt.xlabel("$l$")
    plt.ylabel("$\\frac{l*(l+1)*C_l}{2π}$   [$\mu K^2$]")
    try:
        loc
    except NameError:
        plt.title("Power Spectrum of String Maps")
    else:
        plt.title("Power Spectrum of {}".format(loc))
    plt.legend()
    plt.savefig("powerSpectrum_semilog.png", dpi = 300)
    plt.show()

    # loglog plots
    if dispall.lower() == "y":
        for i in range(C_l_rescaled.shape[0]):
            plt.loglog(elles[0, 1:], C_l_rescaled[i])
    plt.loglog(elles[0, 1:], C_l_rescaled_mean, label = "mean")
    plt.fill_between(elles[0, 1:], C_l_rescaled_mean +  C_l_rescaled_std, C_l_rescaled_mean -  C_l_rescaled_std, alpha = 0.4, label = "1$\sigma$")
    plt.xlabel("$l$")
    plt.ylabel("$\\frac{l*(l+1)*C_l}{2π}$   [$\mu K^2$]")
    try:
        loc
    except NameError:
        plt.title("Power Spectrum of String Maps")
    else:
        plt.title("Power Spectrum of {}".format(loc))
    plt.legend()
    plt.savefig("powerSpectrum_loglog.png", dpi = 300)
    plt.show()

    return

if __name__ == "__main__":
    print("\n-------------------------------------------------------\n")
    print("Angular Power Spectrum Calculator for CMBSIM\n")
    print("\n-------------------------------------------------------\n")
    print("Temperature Units in microKelvin.  Other settings are in header.py.")

    loc = input("\nEnter the path to the maps: ")
    maps = (np.load(loc))
    # dispall = input("Display power spectrum of each individual map? [y/n]: ")
    # print(maps.shape)

    res = header.res
    res_rad = res * arcmin
    plot_C_l_rescaled(maps, res_rad)
