'''header.py
This file contains all the global parameters as well as the default settings. 
It make be upgraded to a library structure so as to allow for many defaults.
'''

# global parameters
STRINGSPERHUBBLE = 3 #the number of strings per Hubble volume
N_HubbleTimeSteps = 15 #defining a constant for the Hubble steps.
TCMB = 2.7255e6 #in micro Kelvin
arcmin = 3.141592653589793 / 180 / 60

# these are the default settings for the program
window = 512 # 1024
res = 1.0 # 0.42 # angular resolution in arcmin
inputCls = "class_scalCls2-76K.dat"
with_noise = "n"
with_strings = "y"
Gmu = 1e-7
runs = 50
disp = "n"
torus = "n"
pSpectrum = "y"
# dispall = "y"
