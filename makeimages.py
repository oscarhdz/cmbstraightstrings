#this program contains the code needed to validate the python program, let's begin
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image as img
import numpy.fft as fft

def sigmoid(x):
    return 1/(1+np.e**-x)

A=np.load('''maps/stringLocMaps.npy''')
B=np.load('''maps/stringMaps.npy''')
#tmap=fft.fft2(A,(512,512),norm=None)
i = 200
j = 30
Ix = np.arange(-i, A.shape[1]-i)
Iy = np.arange(-j, A.shape[0]-j)
Ap = A[:,Ix]
print(Ap.shape)
Bp = B[:,Ix]
a=img.fromarray(255*Ap[0,:,:]/np.amax(A[0]))
a.show()
# b=img.fromarray(255*sigmoid(B/np.amax(B)))
# b.show()
b=img.fromarray(255*sigmoid(Bp[0,:,:]/np.amax(B[0])))
b.show()
# plt.imshow(A)
# plt.show()