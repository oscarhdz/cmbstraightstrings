CMBstraightStrings

A Python 3 CMB anisotropy simulator with cosmic strings based on 
a scale invariant analytic model of long straight strings 
described in Perivolaropoulos L., 1993, Phys. Lett. B, 298, 305. 
This Python 3 program is the evolution of the C program CMBEdge but without
any of the edge detection based on the Canny algorithm. We have used
this simulator in our in cosmic string detection work described in 
arXiv:1810.1188, arXiv:1708.08878, arXiv:1706.04131.  

Oscar Hernandez, Panagiotis Kaloyannis, 2019

To run the program do one of he following:
1. ```python main.py```
The default settings are in header.py

2. ```python powerspectrum.py```
allows one to calculate the angular power spectrum of a map 
or collection of maps in numpy .npy format. 

The standard Anaconda for python 3 will work with one additional installation: 
This installation corresponds to tqdm, the progress bar and timer. It is in
genstringmap and genstringtorus. It can easily removed by changing the line to 
read "for i in range(runs):" 
To install this tqdm:
```conda install -c conda-forge tqdm```

