'''main.py
ported to Python in 2019 by Peter Kaloyannis from main.c
This file contains the main program and all inputs required to generate guassian maps
It was adapted from C to Python in 2019 by Peter Kaloyannis, from a program by Stephen Amsel and Joshue Berger in 2007,
Andrew Stewart in 2008, Oscar Hernandez from 2010-2017 and Razvan Ciuca in 2017.'''

import simulation as sim
import header
import gengaussianmap as ggm
import powerspectrum as ps
import numpy as np
import os
import time
# import tqdm

#Global parameters like STRINGSPERHUBBLE and HUBBLETIMESTEPS and the defaults are in header.py
STRINGSPERHUBBLE = header.STRINGSPERHUBBLE
N_HubbleTimeSteps = header.N_HubbleTimeSteps
TCMB = header.TCMB

if __name__ == "__main__":
    #creation of the test map saving directory
    if os.path.isdir("maps") != True:
        os.mkdir("maps")

    print("-------------------------------------------------------")
    print("| Simulation Details                                  |")
    print("-------------------------------------------------------\n")
    print("We will now collect the parameters for the Gaussian generation process. ",
          "\nDetails on the parameters can be found in the comments of the notes.")
    print("\nWould you like to use a the default settings found in the header.py?\n")

    #printing defaults
    window = header.window
    print("Window size = ",window)
    res = header.res
    print("Resolution = ",res)
    inputCls = header.inputCls
    print("CLS file = ",inputCls)
    with_noise = header.with_noise
    print("With noise? ",with_noise)
    with_strings = header.with_strings
    print("With strings? ",with_strings)
    Gmu = header.Gmu
    if with_strings.lower() == "y":
        print("String Tension = ",Gmu)
    torus = header.torus
    print("On Torus? ",torus)
    runs = header.runs
    print("Number of runs: ",runs)
    if runs <=3 :
        disp = header.disp
    else:
        disp = "n"
    print("Display images? ",disp)
    pSpectrum = header.pSpectrum
    print("Compute the power spectrum? ", pSpectrum)
    # if pSpectrum.lower() == "y":
    #    dispall = header.dispall
    #    print("Display individual map powerspectrums? ",dispall)

    default = input("\n[y/n]: ")
    if default.lower() == "y":
        #reading the Cl values for the simulation to check for errors right away
        Cls=ggm.readCl(inputCls) 

    if default.lower() != "y":
        window = input("\nEnter Window length in pixels: ") #pixels on one side of an image, e.g. 512x512->512
        window = int(window)
        res = input("Resolution (Arcminutes/pixel): ")#self explanatory
        res = float(res)
        inputCls = str(input("Cl file name: "))#Path to the fourier Coefficient table
        Cls=ggm.readCl(inputCls) #reading the Cl values for the simulation to check for errors right away
        with_noise = str(input("With Noise? [y/n]: ")) #self explanatory
        with_strings = str(input("With strings? [y/n]: "))
        if with_strings == "y":
            Gmu = input("Enter the string tension: ")
            Gmu = float(Gmu)
            torus = input("Simulate the strings on a torus? [y/n]: ")
            pSpectrum = input("Compute the power spectrum of strings? [y/n]: ")
            # if pSpectrum.lower() == "y":
            #    dispall = input("Display individual map powerspectrums? [y/n]: ")
        else: 
            Gmu = 0 #this just removes the strings
            torus = "n"
            pSpectrum = "n"
        runs = input("Enter the number of runs to preform: ")
        runs = int(runs)
        if runs <= 3:
            disp = str(input("Since we have less than 3 runs, should I display the images?  [y/n]:"))
        else: disp = 'n'
        

    print("\n-------------------------------------------------------")
    print("| Output Details                                      |")
    print("-------------------------------------------------------\n")

    # write_maps = str(input("Write component maps to a file? [y/n]: "))
    write_maps = 'y'
    # write_sim_maps = str(input("Write full, gauss+string+noise, maps to file? [y/n]: "))
    write_sim_maps = 'y'
    # if write_sim_maps == "y":
    #    write_location = str(input("Enter the file name for the full maps (e.g. maps/test->maps/test.npy): "))
    write_location = 'maps/full'
    print("\n-------------------------------------------------------\n")
    print("Generating Maps\n")

    start=time.time() # start a timer for a speed check

    #preallocating memory to store the runs
    imaps = np.zeros([runs,window,window])
    gmaps = np.zeros([runs,window,window])
    testmaps = np.zeros([runs,window,window])
    if with_strings.lower() == "y":
        smaps = np.zeros([runs,window,window])
        slocmaps = np.zeros([runs,window,window])
    if with_noise == "y":
        noises = np.zeros([runs,window,window])

    #This is a significant change from CMBEDGE so that the
    #array of CLS is only computed once and passed in. PK 2019
    print("Initializing the C_l array.\n")

    #here we build the top half of the CLS array
    L = res * window * np.pi / 10800
    len = Cls.shape[0]
    midpoint = int(window/2)

    UpMap = ggm.upmap(window, Cls, len, L)

    print("Done! Starting individual map generation.\n")

    # for i in tqdm.tqdm(range(runs)):
    for i in range(runs):
        #runnign simulations
        imap, gmap, smap, slocmap, noise = sim.simulation(Cls, window, res, with_strings, Gmu, STRINGSPERHUBBLE, 
                                with_noise, write_maps, i, disp, UpMap, L, midpoint, torus)
        #storing the different maps
        imaps[i] = imap #total maps
        gmaps[i] = gmap #gaussian maps
        if with_strings.lower() == "y":
            smaps[i] = smap #string maps
            slocmaps[i] = slocmap #string location maps
        if with_noise == "y":    
            noises[i] = noise #noise maps
    
    #this writes the individual maps
    if write_maps.lower() =="y":
        string_filename = "maps/stringMaps.npy"
        out_filename = "maps/totalMaps.npy"
        gaussian_filename = "maps/gaussMaps.npy"
        stringsonly_filename = "maps/stringLocMaps.npy"

        # np.save("testmaps.npy",testmaps)
        np.save(gaussian_filename, gmaps)
        if with_noise.lower() == "y": np.save("maps/noiseMaps", noises)
        if with_strings.lower() == "y": 
            np.save(string_filename, smaps)
            np.save(stringsonly_filename, slocmaps)

    # This saves the combinations of the maps
    if write_sim_maps.lower() == 'y':
        np.save("{}.npy".format(write_location),imaps)

    # This computes the power spectrum for all the computed maps
    if pSpectrum.lower() == "y":
        print("\n-------------------------------------------------------\n")
        print("Computing Power Spectrum\n")
        res_rad = res * header.arcmin #unit conversion
        ps.plot_C_l_rescaled(smaps, res_rad)

    end=time.time() #endtime

    print("Time taken to execute: {}s".format(end-start))


    print("\n-------------------------------------------------------")
    print("| Great success                                       |")
    print("-------------------------------------------------------\n")
