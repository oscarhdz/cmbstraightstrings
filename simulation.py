'''simulation.py
ported to Python in 2019 by Peter Kaloyannis from
 * #  simulation.c                   #
 * ##################################
 * This file contains a function to generate a simulated CMB temperature
 * anisotropy map based on the users input. A Gaussian map is generated first,
 * however the addition of noise and or strings, can also be performed as desired.
 *
 * 2007 - Stephen Amsel, Joshua Berger
 * 2008 - Andrew Stewart
 * 2016 - Razvan Ciuca, Oscar Hernandez
 * 2017 - Razvan Ciuca: changed the initialization of stringsonly_map to Matrix and
                        signature of drawstring from imatrix to matrix
 */
This file contains the actual simulation process for generating a single CMB string simulation
by putting together the methods found in gengaussianmap, genstringmap.'''

import numpy as np
import gengaussianmap as ggm
import genstringmap as gsm
import genstringtorus as gst
from PIL import Image as img

def sigmoid(x):
    return 1/(1+np.e**-x)

def simulation(Cls, window, res, with_strings, Gmu, string_h, with_noise,
               write_maps, counter, disp, UpMap ,L ,midpoint, torus):
    #calling gen gaussian for the gaussian portion of the map and preparing the CL Map
    gmap=np.real(ggm.gengaussianmap(UpMap, window, res, L, midpoint))

    output = np.zeros([window,window], dtype = np.float64)
    if with_strings.lower() =="y":

        stringsonly_map = np.zeros([window,window], dtype = np.float64)

        #call genstringmap to make the string portion of the map
        if torus.lower() == "y":
            stringsonly_map, smap = gst.genstringTorus(window,res, 1.0, string_h, stringsonly_map)
        else:
            #add testmap to outputs
            stringsonly_map, smap = gsm.genstringmap(window,res, 1.0, string_h, stringsonly_map)
        
        #scaling to the appropriate Gmu
        alpha=np.sqrt(1 - 7.337475e12 * Gmu**2)

        if alpha < 0:
            print("Warning: String tension too high!\n")
        
        output += Gmu*smap + alpha*np.real(gmap)
    else:
        output = np.real(gmap)
        smap = None
        stringsonly_map = None
    
    if with_noise.lower() =="y":
        # call gennoise for the uniformly distributed noise
        noise = ggm.gennoise(window)
        output += noise
    else:
        noise = None

    #This command displays all the maps
    #It is only available if the number of maps is less than 3 for obvious reasons
    # PK 2019
    if disp.lower() == 'y':
        a=img.fromarray(255*sigmoid(output/np.amax(output)))
        a.show(title= "Total Map {}".format(counter))
        
    return output, gmap, smap, stringsonly_map, noise #, testmap