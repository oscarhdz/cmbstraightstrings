'''genstringmap.py
ported to Python in 2019 by Peter Kaloyannis from
 * #  genstringmap.c                #
 * ##################################
 * This file contains functions to generate the entire map of temperature
 * anisotropies induced by the cosmic string network as well as to draw the
 * temperature anistropy for a single string. The simulation technique is from
 * Moessner et al., "A Cosmic String Specific Signature on the Cosmic Microwave
 * Background", Astrophys. J. 425 (1994). A detailed explanation of the method
 * is given in that paper. Big thanks to Eric Thewalt who re-wrote the
 * conditional statements used to draw the temperature shift rectangles in
 * "drawstring" from scratch, as well as pointing out some additional bugs.
 *
 * 2007 - Stephen Amsel, Joshua Berger
 * 2008 - Andrew Stewart
 * 2016-2017 - Oscar Hernandez:
        changed column-major-format for drawstring() to match row-major-format used by FFTW and rest of program
        added string only line map in drawstring() and passed through to genstringmap(...,imatrix *stringsonly)
 * 2017 Razvan : Added new version of the code which draws the strings_only map, this new code draws string with a
 * gaussian thickness profile, this is done because it makes it easier for neural nets to learn from thick strings
 *      changed drawstring signature to use matrix instead of imatrix. this change also reflected in global.h, simulation.c
 *      changed the code in drawstring which draws the string_only_map to draw strings proportionally to their gradient
 *
 '''


import numpy as np
import numpy.random as rand
import math as m
import tqdm

N_HubbleTimeSteps=15 #defining a constant for the hubble steps.

def genstringmap(window,res,Gmu,string_h,stringsonly):
    '''
    genstringmap creates a simulation space and expands it over different Hubble steps. 
    In each one of these steps a uniform random number decides if each pixel
    in the simulation space has a string. If the criteria is met than a string 
    is drawn on the output map.

    - window is the window resolution
    - res is the angular resolution in arcminutes/pix
    - string_h is how many strings exist per hubble volume
    - stringsonly is a map passed in that has only the string locations, it will be removed soon as an input
    '''
    strings=0 #counter for bugtestings

    crtwo = 2**(1/3)          #def cube root fo 2
    res = res/60              #angular res in deg/pixel
    theta = window*res        #angular width of the window
 
    thetaH = np.zeros(N_HubbleTimeSteps)  #Angular Hubble size at time N_HubbleTimeSteps times, from last scattering to observation
    pixH = np.zeros(N_HubbleTimeSteps)    #H pixel scale
    n = np.zeros(N_HubbleTimeSteps)       #String number Density

    output=np.zeros([window,window], dtype = np.float64) # defining the output array

    for l in tqdm.tqdm(range(N_HubbleTimeSteps)): # filling thetaH
    # for l in range(N_HubbleTimeSteps):
        if l==0:
            thetaH[l]=1.8
        else:
            thetaH[l]=crtwo*thetaH[l-1]
    
        # now we find the hubble volume in pixels
        pixH[l]=int(m.floor(thetaH[l]/res))
        
        #extend the simulated region by a hubble volume in each direction *sqrt(2) since the strings can be @45 deg.
        extwindow=2*m.floor(pixH[l]*np.sqrt(2))+window

        #compute the number of strings that should be laid down in the network via the scaling solution
        n[l]=string_h*(2**1.5 + (theta/thetaH[l]))**2 #as cited in CMBEDGE

        #compute the probability that any pixel is the midpoint of a string
        prob=n[l]/extwindow**2 #considering multiplying this by two since we have about half the strings. something still doesn't seem right though
        #generate a random number for each pixel in the simulated region and if that number
        #is less than the probability draw a string centered on that pixel
        #creating the random number fields
        rand1 = rand.rand(extwindow,extwindow) #for string positions
        rand2 = rand.rand(extwindow,extwindow) #for string slopes

        StrLoc = np.argwhere(rand1<prob)
        for i in range(StrLoc.shape[0]):
            stringsonly, output= drawstring(StrLoc[i,0]-int(pixH[l]*np.sqrt(2)),StrLoc[i,1]-int(pixH[l]*np.sqrt(2)),output,stringsonly,
                                            pixH[l],Gmu,rand2[StrLoc[i,0],StrLoc[i,1]])
            strings += 1

    return stringsonly, output 


def drawstring(i,j, output, strings_address, pixH, Gmu, random):
    '''
    drawstring draws a straight string string and string location to the output map
    by rendering highly simplified "boxed" string effects. For more details refer to
    the paper at the top of this page.
    
    - i,j are the position of the strings
    - ouput is the output array
    - strings_address is the string locations
    - pixH is the current angular size of the hubble volume
    - Gmu is the string tension
    - random is a random number used to assing the slope
    '''
    deltaX = 0 #for the flip of the array that comes later
    deltaXmin = 0
    deltaXmax = 0
    vsgs = 0.15 #velocity of string * relativistic gamma
    angle = np.pi*random #give the string a random orientation
    
    slope1 = np.tan(angle)
    if slope1>0:
        o=1
    else:
        o=-1
    slope2 = 1/slope1
    
    shifts = np.sin(angle)*pixH
    shiftc = np.cos(angle)*pixH

    #allowing positive and negative orientations
    if rand.rand() > 0.5:
        r=rand.rand()
    else: 
        r=-rand.rand()

    beta = 4*np.pi*Gmu*vsgs #calculate the magnitude of the temperature fluctuations
    
    '''
	Draw rectangles on each side of the string, one with a positive 
	temperature shift and the other with a negative temperature shift. 
	The rectangles extend the length of the string (i.e. 2*pixH) 
	and one Hubble radius perpendicular to the string (i.e. pixH).
	Placed in a cartesian map, the two rectangles together span from some xmin 
	to some xmax. This span is delimited by the edges of the rectangles and/or 
	the sides of the window (in case the rectangles extended farther than the window). 
	Anything outside of that span is ignored to save calculation time.
    '''

    #finding xmin and xmax and bounding indices to the array
    #this does not seem true to the method but ok bigger box whatever??
    xmin = int(m.floor(i-abs(shifts)-abs(shiftc)))
    if xmin < 0: xmin=0
    xmax = int(m.floor(i+abs(shifts)+abs(shiftc)))
    if xmax > output.shape[0]-1: xmax = output.shape[0]-1
    # print(xmax-xmin)
    #find the O.H. for the string and bounding the indecies to the array
    string_xmin = int(m.floor(i-abs(shiftc)))
    if string_xmin < 0: string_xmin=0
    string_xmax = int(m.floor(i+abs(shiftc)))
    if string_xmax > output.shape[0]-1: string_xmax = output.shape[0]-1
    # print(string_xmin,string_xmax)

    #since we are creating the negative slope strings by flipping an array for which only a part is rendered
    #this means the flip is not properly centered and shifts the string away from its corresponding string location
    #these terms adjust for this in the flipped case
    if o<=0:
        #translation of the string needed for the later mirroring
        deltaXmin = string_xmin - int(m.floor(i-abs(shiftc))) #shift on the left
        deltaXmax = string_xmax - int(m.floor(i+abs(shiftc))) #shift on the right
        deltaX = deltaXmax + deltaXmin #total shift
        width = string_xmax - string_xmin #width of the string
    
    #here we lay the ground work to initialize an array that will have the string location
    corner1_x = float(i - abs(shiftc))+1
    corner2_x = float(i + abs(shiftc))+1
    corner1_y = float(j - abs(shifts))
    corner2_y = float(j + abs(shifts))

    # print(corner1_x,corner2_x,corner1_y,corner2_y)
    #creating the counters that are essential for mapping the string values to the output array
    min_counter1 = int(m.floor(corner1_x))+deltaXmin #shifting things on the left
    max_counter1 = int(m.floor(corner2_x))+deltaXmax #shifting things on the right
    # print(min_counter1, max_counter1, deltaX)
    min_counter2 = int(m.floor(corner1_y))
    max_counter2 = int(m.floor(corner2_y))

    #apply conditions so that the indices to not exceed the array\
    if o <= 0:
        if min_counter1 < 0: max_counter1 = max_counter1+deltaX
        min_counter1 = max_counter1-width


    else:
        if min_counter1 < 0: min_counter1=0 
        if max_counter1 > strings_address.shape[0]-1: max_counter1 = strings_address.shape[0]-1

    #vertical boundary conditions
    if min_counter2 < 0: min_counter2=0
    if max_counter2 > strings_address.shape[0]-1: max_counter2 = strings_address.shape[0]-1

    # Now we call to generate the array containing the string
    if max_counter1 <= min_counter1 or max_counter2 <= min_counter2:
        pass
    else:
        strings_address = genstringonlyArray(deltaX, max_counter1, max_counter2, min_counter1, min_counter2,
                                             string_xmax, string_xmin,
                                             corner1_x, corner2_x, corner1_y, corner2_y, strings_address,r,o)
    
    #checking if the string is even intersecting the map
    if xmax-xmin>=0:
        coords, sizeY = stringeffectsize(i,j, xmin, xmax, shifts, shiftc, slope1, slope2, output)
        output = drawstringeffectsArray(xmin,xmax, sizeY, coords, output, beta, r ,o)
    return strings_address, output

#This is the faster numpy version of genstringonlymap
def genstringonlyArray(deltaX, max_counter1, max_counter2, min_counter1, min_counter2,
                       string_xmax, string_xmin,
                       corner1_x, corner2_x, corner1_y, corner2_y,  strings_address,r,o):
    '''
    genstringonlyArray creates the string locations by taking the distance of the pixel
    to a string and then taking a gaussian profile of the distances. It needs to be
    adjusted for flipping.

    -i,j is the position of the center of the the string
    -shifts is the height of the string from center
    -shiftc is the width of the string from center
    -string_address 
    '''
    
    std_dev_line_profile=1.0

    #length of any points in the array
    # distx = np.arange(min_counter1-m.floor(corner1_x),max_counter1-m.floor(corner1_x))
    distx = np.arange(min_counter1-m.floor(corner1_x)-deltaX,max_counter1-m.floor(corner1_x)-deltaX)
    disty = np.arange(min_counter2-m.floor(corner1_y),max_counter2-m.floor(corner1_y))

    #length of the string
    maxdistx = corner2_x-corner1_x
    maxdisty = corner2_y-corner1_y

    #creating the appropriate coordinate planes
    X,Y = np.meshgrid(distx,disty)

    #distance between corners of square
    a_norm = np.hypot(maxdistx,maxdisty, dtype= np.float64)
    x_norm = np.hypot(X,Y, dtype= np.float64)

    #computing the dot product of the two vectors
    a_dot_x = X*maxdistx+Y*maxdisty

    #here we fix an anomaly in the code where small distances are negative. Instead we assign them the value 0 -PK
    distsquare = x_norm**2-(a_dot_x/a_norm)**2
    distsquare[distsquare < 0] = 0
    distance_to_line = np.sqrt(distsquare, dtype= np.float64)
    
    gaussian_decrease= abs(r)*np.exp(-((distance_to_line/std_dev_line_profile)**2)/2.0, dtype= np.float64) 
    
    #Filling the array according to the slope
    if o <= 0:
        strings_address[min_counter2:max_counter2,string_xmax:string_xmin:-1] = np.where(gaussian_decrease > strings_address[min_counter2:max_counter2,string_xmax:string_xmin:-1],
                        gaussian_decrease, strings_address[min_counter2:max_counter2,string_xmax:string_xmin:-1])
    else:
        strings_address[min_counter2:max_counter2,min_counter1:max_counter1:1] = np.where(gaussian_decrease > strings_address[min_counter2:max_counter2,min_counter1:max_counter1:1], 
                        gaussian_decrease, strings_address[min_counter2:max_counter2,min_counter1:max_counter1:1])
    return strings_address

#This function returns the size of the string effects array as well as the coordinates for the string effects -PK
def stringeffectsize(i, j, xmin, xmax, shifts, shiftc, slope1, slope2, output):
    '''
    stringeffectsize creates bottom and top coordinates for filling the string effect temperature maps

    - i,j is the position of the string
    - xmin and xmax are the max x and y positions
    - output is the map the put the effects on
    - shiftc and shifts are the displacement at the string tips
    - slope1 and slope2 are the slopes of the string effects 
    '''
    YMAX = output.shape[1] #this is the maximum index
    x = np.arange(xmin, xmax+1)
    
    #the equations of the sides of the rectangle behind the string, evaluated at the given x
    a = slope1*(x-i+shifts)+shiftc+j
    b = slope1*(x-i)+j
    c = slope1*(x-i-shifts)-shiftc+j
    d = -slope2*(x-i-shiftc)+shifts+j #the original C files omit this minus sign. without it these lines don't form a rectangle
    e = -slope2*(x-i+shiftc)-shifts+j #ask oscar if this is okay

    #These make the indices of the arrays used to generate strings
    if slope1 < 0:
        yminDark = np.ceil(np.maximum(np.maximum(a,e),0))
        yminDark = np.where(yminDark>YMAX, YMAX, yminDark)
        ymaxDark = np.floor(np.minimum(np.minimum(b,d),YMAX))+1
        ymaxDark = np.where(ymaxDark<0, 0, ymaxDark) #this is the start of the boundary conditions!

        
        yminBright = np.ceil(np.maximum(np.maximum(b,e),0))
        yminBright = np.where(yminBright>YMAX, YMAX, yminBright)
        
        ymaxBright = np.floor(np.minimum(np.minimum(c,d),YMAX))+1
        ymaxBright = np.where(ymaxBright<0, 0, ymaxBright)
        
        size = np.amax(ymaxBright-yminDark)
    
    else:
        yminDark = np.ceil(np.maximum(np.maximum(c,e),0))
        yminDark = np.where(yminDark>YMAX, YMAX, yminDark)

        ymaxDark = np.floor(np.minimum(np.minimum(b,d),YMAX))+1
        ymaxDark = np.where(ymaxDark<0, 0, ymaxDark)

        yminBright = np.ceil(np.maximum(np.maximum(b,e),0))
        yminBright = np.where(yminBright>YMAX, YMAX, yminBright)
        
        ymaxBright = np.floor(np.minimum(np.minimum(a,d),YMAX))+1
        ymaxBright = np.where(ymaxBright<0, 0, ymaxBright)
        
        size = np.amax(ymaxBright-yminDark)
    
    return [yminDark.astype(np.int), ymaxDark.astype(np.int),
            yminBright.astype(np.int), ymaxBright.astype(np.int)], size

#This function maps the string effects to an array -PK        
def drawstringeffectsArray(xmin,xmax, sizeY, coords, output, beta, r ,o):
    '''
    drawstringeffectsArray uses the coordinates above to add the string 
    effects onto the output map 
    
    - xmin and xmax are the max x and y positions
    - sizeY is the height of the string effects
    - coords are the indexes of the box
    - output is the map the put the effects on
    - beta is the effects
    - r is a speed mulitplier
    - o is to create negative slopes
    '''
    effect = beta*r
    for i in range(xmax+1-xmin):
        #darkfill
        output[coords[0][i]:coords[1][i], xmin+i] -= effect
        #lightfill
        output[coords[2][i]:coords[3][i], xmin+i] += effect 
    return output