'''genstringtorus.py
This file contains the string simulation for repeating boundary conditions,
 this was based off the most recent version of CMBEdge's string simulation
in 2019. This file contains functions to generate the entire map of temperature 
anisotropies induced by the cosmic string network as well as to draw the 
temperature anistropy for a single string. The simulation technique is from 
Moessner et al., "A Cosmic String Specific Signature on the Cosmic Microwave
Background", Astrophys. J. 425 (1994). A detailed explanation of the method 
is given in that paper and has had slight adaptations for the boundary conditions.
'''

#2019 Panagiotis Kaloyannis

import numpy as np
import numpy.random as rand
import tqdm
import math as m
import header
from PIL import Image as img

N_HubbleTimeSteps = header.N_HubbleTimeSteps

#Simulates the strings for a map
def genstringTorus(window,res,Gmu,string_h,stringsonly):
    '''
    - window is the window resolution
    - res is the angular resolution in arcminutes/pix
    - string_h is how many strings exist per hubble volume
    - stringsonly is a map passed in that has only the string locations, it will be removed soon as an input
    '''
    # strings=0 #counter for bugtestings

    crtwo = 2**(1/3)          #def cube root fo 2
    res = res/60              #angular res in deg/pixel
    theta = window*res        #angular width of the window
 
    thetaH = np.zeros(N_HubbleTimeSteps)  #Angular Hubble size at time N_HubbleTimeSteps times, from last scattering to observation
    pixH = np.zeros(N_HubbleTimeSteps)    #H pixel scale
    n = np.zeros(N_HubbleTimeSteps)       #String number Density
    # print("Angular size of winodw: ",theta)
    # print("String Density: ", string_h)
    output=np.zeros([window,window]) #defining the output array

    for l in tqdm.tqdm(range(N_HubbleTimeSteps)): #filling thetaH
        if l==0:
            thetaH[l]=1.8
        else:
            thetaH[l]=crtwo*thetaH[l-1]
    
        #now we find the hubble volume in pixels
        pixH[l]=int(m.floor(thetaH[l]/res))
        #print(pixH[l])
        
        #extend the simulated region by a hubble volume in each direction *sqrt(2) since the strings can be @45 deg.
        extwindow=2*m.floor(pixH[l]*np.sqrt(2))+window
        # print("Window Extension: ", extwindow)

        #compute the number of strings that should be laid down in the network via the scaling solution

        #n[l]=string_h*((thetaH[l]+theta)/thetaH[l])**2 #as cited in simulation documentation
        n[l]=string_h*(2**1.5 + (theta/thetaH[l]))**2 #as cited in CMBEDGE

        #compute the probability that any pixel is the midpoint of a string
        prob=n[l]/extwindow**2 #considering multiplying this by two since we have about half the strings. something still doesn't seem right though
        # print("Probability: ",prob, " in generation ",l,"\n")
        #generate a random number for each pixel in the simulated region and if that number
        #is less than the probability draw a string centered on that pixel
        rand1 = rand.rand(extwindow,extwindow)
        rand2 = rand.rand(extwindow,extwindow)
        # for i in range(extwindow):
        #     for j in range(extwindow):
        #         if rand1[j,i]<prob:
        #             stringsonly, output= drawstring(i-int(pixH[l]*np.sqrt(2)),j-int(pixH[l]*np.sqrt(2)),output,stringsonly,pixH[l],Gmu,rand2[i,j])
        #             # stringsonly, output= drawstring(i,j,output,stringsonly,pixH[l],Gmu,rand2[i,j])
        #             strings += 1
        StrLoc = np.argwhere(rand1<prob)
        for i in range(StrLoc.shape[0]):
            stringsonly, output= drawstringTorus(StrLoc[i,0]-int(pixH[l]*np.sqrt(2)),StrLoc[i,1]-int(pixH[l]*np.sqrt(2)),output,stringsonly,
                                            pixH[l],Gmu,rand2[StrLoc[i,0],StrLoc[i,1]])
            # strings += 1
    # print("Angular size of the hubble volume: ",pixH)
    # print("Number of strings to be laid: ",n)
    # print("Hubble Volume: ",thetaH)
    # print("String Count: {}".format(strings))
    return stringsonly, output

#draws a single string and its contributions
def drawstringTorus(i,j, output, strings_address, pixH, Gmu, random):
    '''
    - i,j are the position of the strings
    - ouput is the output array
    - strings_address is the string locations
    - pixH is the current angular size of the hubble volume
    - Gmu is the string tension
    - random is a random number used to assing the slope
    '''
    vsgs = 0.15 #velocity of string * relativistic gamma
    angle = np.pi*random #give the string a random orientation
    
    windowMax = output.shape[0]
    windowMid = int(output.shape[0]/2)
    
    slope1 = np.tan(angle)
    if slope1>0:
        o=1
    else:
        o=-1
    slope2 = 1/slope1
    
    shifts = np.sin(angle)*pixH
    shiftc = np.cos(angle)*pixH
    # print(i,j)
    # print(random)
    # print(pixH)
    #here we begin building arrays to contain all the contributions of the string and its location
    fullXstrmin = int(-abs(np.floor(shiftc)))
    fullXstrmax = int(abs(np.floor(shiftc)))
    fullYstrmin = int(-abs(np.floor(shifts)))
    fullYstrmax = int(abs(np.floor(shifts)))
    
        #removing strings that don't intercept the image
    if i+fullXstrmax <= 0 or i+fullXstrmin >= windowMax or j+fullYstrmax <= 0 or j+fullYstrmin >= windowMax:
        return strings_address, output
    
    #limiting the render size of the strings to avoid spurious effects
    if fullXstrmax-fullXstrmin > windowMax:
        Xstrmin = -windowMid
        Xstrmax = windowMid
    else:
        Xstrmin = fullXstrmin
        Xstrmax = fullXstrmax
    
    if fullYstrmax - fullYstrmin> windowMax:
        Ystrmin = -windowMid
        Ystrmax = windowMid
    else:
        Ystrmin = fullYstrmin
        Ystrmax = fullYstrmax
        
#     print(Ystrmin)
#     print(Ystrmax)
    
    fullXmin = int(m.floor(-abs(shifts)-abs(shiftc)))
    fullXmax = int(m.floor(abs(shifts)+abs(shiftc)))
    

    
    #these 4 are for non periodic boundary conditions, remember to delete later
    xmin = fullXmin + i%windowMax
    xmax = fullXmax + i%windowMax
    if xmin < 0: xmin=0
    if xmax > output.shape[0]: xmax = output.shape[0]
        
    width = fullXmax-fullXmin
    oShiftX = int(width/2)
#     print(oShiftX)

# This code is essential to the periodic boundary conditions. DO NOT DELETE
    #now we limit the render size so that we don't have spurious effects from the boundary conditions
    #this will also dramatically reduce the runtime
    if width > windowMax:
        width = windowMax
        Xmax = windowMid
        Xmin = -windowMid
        oShiftX = windowMid
    else:
        Xmax = fullXmax
        Xmin = fullXmin
    
    
    #getting the coordinates for the string effects, remember to change xmin and xmax to Xmin and Xmax when switching back
    #don't forget to remove the 1 in coords for torus effects
    coords1, sizeY, oShiftY = stringeffectsizeTorus(Xmin, Xmax,
                                                   shifts, shiftc, slope1, slope2)
    coords = stringeffectsize(i, j, xmin, xmax, shifts, shiftc, slope1, slope2, output)

# This is for the periodic boundary conditions. DO NOT DELETE ANY COMMENTED CODE HERE
#     if sizeY > windowMax:
#         sizeY = windowMax
#         Ymax = oShiftY + windowMid
#         Ymin = oShiftY - windowMid
#         #setting minimum coordinates
#         coords[0][coords[0]<Ymin] = Ymin
#         coords[2][coords[2]<Ymin] = Ymin
#         #setting maximum coordinates
#         coords[1][coords[1]>Ymax] = Ymax
#         coords[1][coords[1]<Ymin] = Ymin
#         coords[3][coords[3]>Ymax] = Ymax
#         coords[3][coords[3]<Ymin] = Ymin
#         for z in range(4):
#             coords[z] = coords[z] - int(oShiftY-windowMid)
#         oShiftY = windowMid
#     else:
#         pass
    if sizeY > windowMax:
        sizeY = windowMax
        Ymax = oShiftY + windowMid
        Ymin = oShiftY - windowMid
        oShiftY = windowMid
    #creating arrays
#     stringeffect = np.zeros([sizeY,width])
#     print(width,sizeY)
    stringloc = np.zeros([sizeY,width])
    # print(stringloc.shape)
#     print(stringloc.shape)
  
    #creating string location indices
    stringXlocmin = Xstrmin + oShiftX 
    stringXlocmax = Xstrmax + oShiftX 
#     print(stringXlocmin)
#     print(stringXlocmax)
    stringYlocmin = int(Ystrmin + oShiftY)
    stringYlocmax = int(Ystrmax + oShiftY)
    # print(stringYlocmin)
    # print(stringYlocmax)
    
    #allowing positive and negative orientations
    if rand.rand() > 0.5:
        r=rand.rand()
    else: 
        r=-rand.rand()

    beta = 4*np.pi*Gmu*vsgs #calculate the magnitude of the temperature fluctuations
    
	# Draw rectangles on each side of the string, one with a positive 
	# temperature shift and the other with a negative temperature shift. 
	# The rectangles extend the length of the string (i.e. 2*pixH) 
	# and one Hubble radius perpendicular to the string (i.e. pixH).
	# Placed in a cartesian map, the two rectangles together span from some xmin 
	# to some xmax. This span is delimited by the edges of the rectangles and/or 
	# the sides of the window (in case the rectangles extended farther than the window). 
	# Anything outside of that span is ignored to save calculation time.

    #This code is for periodic effects DO NOT DELETE
#     stringeffect = drawstringeffectsTorus(stringeffect, coords, beta, r, width)

    #This code is for non periodic string effects. It is temporary. 
    output = drawstringeffectsArray(xmin, xmax, coords, output, beta, r)
    
    stringloc = genstringonlyTorus(fullXstrmin, fullXstrmax, fullYstrmin, fullYstrmax,
                       Xstrmin, Xstrmax, Ystrmin, Ystrmax, windowMax,
                       stringXlocmin, stringXlocmax, stringYlocmin, stringYlocmax,
                       stringloc, r, o)
    #IMPORTANT: For periodic effects, readd stringeffect and output to the inputs and readd output to returns
    strings_address = maptoTorus(i,j, strings_address, stringloc, 
                                         fullXmin, fullXmax, windowMid,
                                         oShiftX,oShiftY)
    return strings_address, output
    
#This function returns the size of the string effects array as well as the coordinates for the string effects
def stringeffectsize(i, j, xmin, xmax, shifts, shiftc, slope1, slope2, output):
    '''
    - i,j is the position of the string
    - xmin and xmax are the max x and y positions
    - output is the map the put the effects on
    - shiftc and shifts are the displacement at the string tips
    - slope1 and slope2 are the slopes of the string effects 
    '''
    YMAX = output.shape[1] #this is the maximum index
    x = np.arange(xmin, xmax+1)
    i = i%YMAX
    j = j%YMAX
    #the equations of the sides of the rectangle behind the string, evaluated at the given x
    a = slope1*(x-i+shifts)+shiftc+j
    b = slope1*(x-i)+j
    c = slope1*(x-i-shifts)-shiftc+j
    d = -slope2*(x-i-shiftc)+shifts+j #the original C files omit this minus sign. without it these lines don't form a rectangle
    e = -slope2*(x-i+shiftc)-shifts+j #ask oscar if this is okay

    #These make the indices of the arrays used to generate strings
    if slope1 < 0:
        yminDark = np.ceil(np.maximum(np.maximum(a,e),0))
        yminDark = np.where(yminDark>YMAX, YMAX, yminDark)
        ymaxDark = np.floor(np.minimum(np.minimum(b,d),YMAX))+1
        ymaxDark = np.where(ymaxDark<0, 0, ymaxDark) #this is the start of the boundary conditions!

        
        yminBright = np.ceil(np.maximum(np.maximum(b,e),0))
        yminBright = np.where(yminBright>YMAX, YMAX, yminBright)
        
        ymaxBright = np.floor(np.minimum(np.minimum(c,d),YMAX))+1
        ymaxBright = np.where(ymaxBright<0, 0, ymaxBright)
        
        #size = np.amax(ymaxBright-yminDark)
    
    else:
        yminDark = np.ceil(np.maximum(np.maximum(c,e),0))
        yminDark = np.where(yminDark>YMAX, YMAX, yminDark)

        ymaxDark = np.floor(np.minimum(np.minimum(b,d),YMAX))+1
        ymaxDark = np.where(ymaxDark<0, 0, ymaxDark)

        yminBright = np.ceil(np.maximum(np.maximum(b,e),0))
        yminBright = np.where(yminBright>YMAX, YMAX, yminBright)
        
        ymaxBright = np.floor(np.minimum(np.minimum(a,d),YMAX))+1
        ymaxBright = np.where(ymaxBright<0, 0, ymaxBright)
        
    
    return [yminDark.astype(np.int), ymaxDark.astype(np.int),
            yminBright.astype(np.int), ymaxBright.astype(np.int)]

def drawstringeffectsArray(xmin,xmax, coords, output, beta, r):
    '''
    - xmin and xmax are the max x and y positions
    - sizeY is the height of the string effects
    - coords are the indexes of the box
    - output is the map the put the effects on
    - beta is the effects
    - r is a speed mulitplier
    - o is to create negative slopes
    '''
    effect = beta*r
    # print(xmax+1-xmin)
    for i in range(xmax-xmin):
        #darkfill
        output[coords[0][i]:coords[1][i], xmin+i] -= effect
        #lightfill
        output[coords[2][i]:coords[3][i], xmin+i] += effect 
    return output

#generates the string locations on the torus
def genstringonlyTorus(fullXstrmin, fullXstrmax, fullYstrmin, fullYstrmax,
                       Xstrmin, Xstrmax, Ystrmin, Ystrmax, windowMax,
                       stringXlocmin, stringXlocmax, stringYlocmin, stringYlocmax,
                       stringloc, r, o):

    '''
    -i,j is the position of the center of the the string
    -shifts is the height of the string from center
    -shiftc is the width of the string from center
    -string_address 
    '''
    
    std_dev_line_profile=1
    
    #length of the string
    maxdistx = (fullXstrmax - fullXstrmin)
    distx = Xstrmax - Xstrmin
    deltaX = int((maxdistx-distx)/2)
    
    maxdisty = (fullYstrmax - fullYstrmin)
    disty = Ystrmax - Ystrmin
    deltaY = int((maxdisty - disty)/2)
    
    #length of any points in the array
    if o<=0 and distx == windowMax:
        distx = np.arange(deltaX, maxdistx - deltaX-1)
    else:
        distx = np.arange(deltaX, maxdistx - deltaX)
    disty = np.arange(deltaY, maxdisty - deltaY)

 
    X,Y = np.meshgrid(distx,disty)
    
    #print(min_counter1,max_counter1,min_counter2,max_counter2)

    #distance between corners of square
    a_norm = np.hypot(maxdistx,maxdisty, dtype= np.float64)
    x_norm = np.hypot(X,Y)

    a_dot_x = X*maxdistx+Y*maxdisty
    #here we fix an anomaly in the code where small distances are negative. Instead we assign them the value 0 -PK
    distsquare = x_norm**2-(a_dot_x/a_norm)**2
    distsquare[distsquare < 0] = 0
    distance_to_line = np.sqrt(distsquare, dtype= np.float64)
    # print(distance_to_line)
    
    gaussian_decrease= abs(r)*np.exp(-((distance_to_line/std_dev_line_profile)**2)/2.0)
#     print(gaussian_decrease.shape)
    #Filling the array according to the slope
    if o <= 0:
        stringloc[stringYlocmin:stringYlocmax,stringXlocmax:stringXlocmin:-1] = gaussian_decrease
    else:
        stringloc[stringYlocmin:stringYlocmax,stringXlocmin:stringXlocmax:1] = gaussian_decrease
    return stringloc

#This function returns the size of the string effects array as well as the coordinates for the string effects
def stringeffectsizeTorus(Xmin, Xmax,
                          shifts, shiftc, slope1, slope2):
    '''
    - i,j is the position of the string
    - xmin and xmax are the max x and y positions
    - output is the map the put the effects on
    - shiftc and shifts are the displacement at the string tips
    - slope1 and slope2 are the slopes of the string effects 
    '''
    x = np.arange(Xmin, Xmax)
    
#     #the equations of the sides of the rectangle behind the string, evaluated at the given x
#     a = slope1*(x-oShiftX+shifts)+shiftc 
#     b = slope1*(x-oShiftX)
#     c = slope1*(x-oShiftX-shifts)-shiftc 
#     d = -slope2*(x-oShiftX-shiftc)+shifts  #the original C files omit this minus sign. without it these lines don't form a rectangle
#     e = -slope2*(x-oShiftX+shiftc)-shifts #ask oscar if this is okay

    #the equations of the sides of the rectangle behind the string, evaluated at the given x
    a = slope1*(x+shifts)+shiftc 
    b = slope1*(x)
    c = slope1*(x-shifts)-shiftc 
    d = -slope2*(x-shiftc)+shifts  #the original C files omit this minus sign. without it these lines don't form a rectangle
    e = -slope2*(x+shiftc)-shifts #ask oscar if this is okay

    #These make the indices of the arrays used to generate strings
    if slope1 < 0:
        yminDark = np.ceil(np.maximum(a,e))
        ymaxDark = np.floor(np.minimum(b,d))+1
        
        yminBright = np.ceil(np.maximum(b,e))
        ymaxBright = np.floor(np.minimum(c,d))+1
        
    
    else:
        yminDark = np.ceil(np.maximum(c,e))
        ymaxDark = np.floor(np.minimum(b,d))+1

        yminBright = np.ceil(np.maximum(b,e))
        ymaxBright = np.floor(np.minimum(a,d))+1
    
    size = max(abs(ymaxBright.max() - yminDark.min()), abs(ymaxDark.max() - yminBright.min())).astype(np.int)
#     size = np.amax(ymaxBright-yminDark).astype(np.int) 
    oShiftY = np.floor(size/2)
    ymaxDark[ymaxDark<-oShiftY] = -oShiftY
        
    return [(yminDark+oShiftY).astype(np.int), (ymaxDark+oShiftY).astype(np.int),
            (yminBright+oShiftY).astype(np.int), (ymaxBright+oShiftY).astype(np.int)], size, oShiftY

#draws the string effects for the torus
def drawstringeffectsTorus(stringeffect, coords, beta, r, width):
    '''
    - xmin and xmax are the max x and y positions
    - sizeY is the height of the string effects
    - coords are the indexes of the box
    - output is the map the put the effects on
    - beta is the effects
    - r is a speed mulitplier
    - o is to create negative slopes
    '''
    effect = beta*r
    for i in range(width):
        #darkfill
        stringeffect[coords[0][i]:coords[1][i], i] -= effect
        #lightfill
        stringeffect[coords[2][i]:coords[3][i], i] += effect 
    return stringeffect

#this function maps any i,j to a point on the map and divides up the string maps accordingly.
def maptoTorus(i,j, strings_address, stringloc, fullXmin, fullXmax, windowMid,
               oShiftX,oShiftY):
    #IMPORTANT: For periodic effects, readd stringeffect and output to the inputs and readd output to returns
    '''
    - i,j  are the location of the center of the string
    - output is the string effects map
    - strings_adress is the string location map
    - stringloc is the location of the individual string
    - stringeffect is the effect of the individual string
    Since stringloc and stringeffect have the same size, they will be divided the same!
    We will do an initial attempt using for loops!
    '''
    #we begin by creating an array with the position of each pixel of the string effect/location on the
    #2-d plane. This will be better documented when I make the documentation.
    if oShiftX < windowMid:
        Xrange = np.arange(i-oShiftX, i+oShiftX+1)
    #     print(Xrange)
    #     print(i)
#         Yrange = np.arange(j-oShiftY, j+oShiftY)
    else:
        Xrange = np.arange(i-oShiftX, i+oShiftX)
    #     print(Xrange)
    #     print(i)
#         Yrange = np.arange(j-oShiftY, j+oShiftY)
    
    if 2*oShiftY < stringloc.shape[0]:
        Yrange = np.arange(j-oShiftY, j+oShiftY+1)
    else:
        Yrange = np.arange(j-oShiftY, j+oShiftY)
    
    Xpos, Ypos = np.meshgrid(Xrange,Yrange)
    #applying periodic boundary conditions
    Xpos = np.mod(Xpos,strings_address.shape[0]).astype(np.int)
    Ypos = np.mod(Ypos,strings_address.shape[0]).astype(np.int)
    # print(Xpos.shape, Ypos.shape)
    # print(stringloc.shape)
    
    #output[Ypos,Xpos] += stringeffect
    strings_address[Ypos,Xpos] = np.where(strings_address[Ypos,Xpos] < stringloc, stringloc,
                                          strings_address[Ypos,Xpos])
    return strings_address #output
    
    